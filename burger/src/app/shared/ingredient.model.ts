export class Ingredient {
  constructor(
    public ingredientName:string,
    public quantity: number,
    public cost: number,
  ) {
  }

  getPrice(){
    return this.quantity * this.cost;
  }

}

import {Component, Input} from '@angular/core';
import {faBacon, faCloudMeatball, faLeaf, faTrash, faCheese} from '@fortawesome/free-solid-svg-icons';
import {Ingredient} from "../shared/ingredient.model";

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent{
  faTrash = faTrash;
  faCloudMeatball = faCloudMeatball;
  faCheese = faCheese;
  faBacon = faBacon;
  faLeaf = faLeaf;

  meatNumber = '0';
  cheeseNumber = '0';
  saladNumber = '0';
  baconNumber = '0';

  @Input() ingredients: Ingredient[]=[];

}
